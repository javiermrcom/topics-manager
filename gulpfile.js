var gulp = require('gulp'),
    sass = require('gulp-sass'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    cleanCSS = require('gulp-clean-css'),
    webserver = require('gulp-webserver');

var paths = {
    'vendor': 'angular/node_modules/',
    'app': 'angular/scripts/',
    'assets': 'angular/assets/'
};

var destPaths = {
    'styles': 'public/stylesheets',
    'scripts': 'public/javascripts'
};

gulp.task('vendor:scripts', vendorScripts);
gulp.task('scripts', scripts);
gulp.task('styles', styles);
gulp.task('webserver', serve);
gulp.task('watch', watch);
gulp.task('run', run);

// --------------------------

function run() {
    gulp.start('vendor:scripts', 'scripts', 'styles' /*,'webserver'*/, 'watch');
}

function vendorScripts() {
    return gulp.src([
        paths.vendor + 'angular/angular.min.js',
        paths.vendor + 'angular-animate/angular-animate.js',
        paths.vendor + 'jquery/dist/jquery.min.js',
        paths.vendor + 'angular-ui-router/release/angular-ui-router.min.js'
    ])
        .pipe(concat('vendor.js'))
        .pipe(uglify())
        .pipe(gulp.dest(destPaths.scripts))
}

function scripts() {
    return gulp.src([
        paths.app + 'main.js',
        paths.app + '**/*.module.js',
        paths.app + '**/*.model.js',
        paths.app + '**/*.service.js',
        paths.app + '**/*.factory.js',
        paths.app + '**/*.filter.js',
        paths.app + '**/*.directive.js',
        paths.app + '**/*.component.js',
        paths.app + '**/*.util.js',
        paths.app + '**/*.controller.js',
        paths.app + '**/*.routes.js',
        paths.app + '**/*.interceptor.js'
    ])
        .pipe(concat('app.js'))
        .pipe(jshint(jshint_config))
        // .pipe(uglify())
        .pipe(gulp.dest(destPaths.scripts))
}


function styles() {
    return gulp.src(paths.assets + 'scss/*.scss')
        .pipe(sass({style: 'compressed'}).on('error', sass.logError))
        .pipe(concat('styles.css'))
        .pipe(cleanCSS())
        .pipe(gulp.dest(destPaths.styles))
}


function serve() {
    gulp.src('public')
        .pipe(webserver({
            port: 8080,
            livereload: true,
            directoryListing: false,
            open: true
        }));
}

function watch() {
    gulp.watch(paths.assets + 'scss/**/*.scss', ['styles']);
    gulp.watch(paths.app + '**/*.js', ['scripts']);
}

var jshint_config = {
    "bitwise": true,
    "camelcase": true,
    "curly": true,
    "eqeqeq": true,
    "es3": false,
    "forin": true,
    "freeze": true,
    "immed": true,
    "indent": 4,
    "latedef": "nofunc",
    "newcap": true,
    "noarg": true,
    "noempty": true,
    "nonbsp": true,
    "nonew": true,
    "plusplus": false,
    "quotmark": "single",
    "undef": true,
    "unused": false,
    "strict": false,
    "maxparams": 10,
    "maxdepth": 5,
    "maxstatements": 40,
    "maxcomplexity": 8,
    "maxlen": 120,

    "asi": false,
    "boss": false,
    "debug": false,
    "eqnull": true,
    "esnext": false,
    "evil": false,
    "expr": false,
    "funcscope": false,
    "globalstrict": false,
    "iterator": false,
    "lastsemic": false,
    "laxbreak": false,
    "laxcomma": false,
    "loopfunc": true,
    "maxerr": false,
    "moz": false,
    "multistr": false,
    "notypeof": false,
    "proto": false,
    "scripturl": false,
    "shadow": false,
    "sub": true,
    "supernew": false,
    "validthis": false,
    "noyield": false,

    "browser": true,
    "node": true,

    "globals": {
        "angular": false,
        "$": false
    }
};