angular.module('topicsManager', [
    'ui.router'
]);
angular
    .module('topicsManager')
    .factory('topicsService', topicsService);

topicsService.$inject = ['$http'];
function topicsService($http) {

    var service = {
        'topics': [],
        'getAll': getAll,
        'create': create,
        'upvote': upvote,
        'get': get,
        'addComment': addComment
    };

    return service;

    // ------------------------------------

    function getAll() {
        return $http.get('/topics')
            .then(function (data) {
                console.log(data.data);
                angular.copy(data.data, service.topics);
            });
    }

    function create(topic) {
        return $http.post('/topics', topic)
            .then(function (data) {
                service.topics.push(data);
            });
    }

    function upvote(topic) {
        return $http.put('/topics/' + topic._id + '/upvote')
            .then(function (data) {
                topic.upvotes += 1;
            });
    }

    function get(id) {
        return $http.get('/topics/' + id)
            .then(function (res) {
                return res.data;
            });
    }

    function addComment(id, comment) {
        return $http.post('/topics/' + id + '/comments', comment);
    }
}
angular
    .module('topicsManager')
    .controller('MainCtrl', MainCtrl);

MainCtrl.$inject = ['topics', 'topicsService'];
function MainCtrl(topics, topicsService) {
    var vm = this;

    vm.addTopic = addTopic;
    vm.vote = vote;
    vm.topics = topicsService.topics;

    // -----------------------------------

    function addTopic() {
        if (!vm.title || vm.title == '') return;

        topicsService.create({
            title: vm.title && vm.title != '' ? vm.title : 'No title',
            link: vm.link && vm.link != '' ? vm.link : null,
            upvotes: 0
        });

        vm.title = '';
        vm.link = '';
    }

    function vote(topic) {
        topicsService.upvote(topic);
    }
}

angular
    .module('topicsManager')
    .controller('TopicCtrl', TopicCtrl);

TopicCtrl.$inject = ['topic', 'topicsService'];
function TopicCtrl(topic, topicsService) {
    var vm = this;

    vm.topic = topic;

    vm.vote = vote;
    vm.addComment = addComment;

    // -----------------------------------

    function vote(comments) {
        comments.upvotes++;
    }

    function addComment() {
        if (!vm.body || vm.body == '') return;

        topicsService.addComment(vm.topic._id, {
            body: vm.body,
            author: 'user'
        }).then(function (data) {
            console.log(data.data);
            vm.topic.comments.push(data.data);
        });

        vm.body = '';
    }

}

angular
    .module('topicsManager')
    .config(routes);

routes.$inject = [
    '$stateProvider',
    '$urlRouterProvider'
];
function routes($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: '/views/home.tpl.html',
            controller: 'MainCtrl',
            controllerAs: 'vm',
            resolve: {
                topics: function (topicsService) {
                    return topicsService.getAll();
                }
            }
        })
        .state('topics', {
            url: '/topics/:id',
            templateUrl: '/views/topic.tpl.html',
            controller: 'TopicCtrl',
            controllerAs: 'vm',
            resolve: {
                topic: function (topicsService, $stateParams) {
                    return topicsService.get($stateParams.id);
                }
            }
        });

    $urlRouterProvider.otherwise('home');
}
