var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CommentSchema = new Schema({
    body: String,
    author: String,
    upvotes: {type: Number, default: 0},
    topic: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Topic' }]
});

CommentSchema.methods.upvote = function(cb) {
    this.upvotes += 1;
    this.save(cb);
};

var Comment = mongoose.model('Comment', CommentSchema);

module.exports = Comment;