var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TopicSchema = new Schema({
    title: String,
    link: String,
    upvotes: {type: Number, default: 0},
    comments: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Comment' }]
});

TopicSchema.methods.upvote = function(cb) {
    this.upvotes += 1;
    this.save(cb);
};

var Topic = mongoose.model('Topic', TopicSchema);

module.exports = Topic;