var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
require('../models/Topic');
require('../models/Comment');
var Topic = mongoose.model('Topic');
var Comment = mongoose.model('Comment');

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {title: 'Express'});
});

/* GET topics */
router.get('/topics', function (req, res, next) {
    Topic.find(function (err, topics) {
        if (err) return next(err);

        res.json(topics);
    })
});

/* GET topic */
router.get('/topics/:topic', function (req, res, next) {
    req.topic.populate('comments', function(err, topic) {
       if (err) return next(err);

        res.json(topic);
    });
});

/* POST topics */
router.post('/topics', function (req, res, next) {
    var topic = new Topic(req.body);

    topic.save(function (err, post) {
        if (err) return next(err);

        res.json(post);
    })
});

/* PUT topic upvote */
router.put('/topics/:topic/upvote', function (req, res, next) {
    req.topic.upvote(function (err, topic) {
        if (err) return next(err);

        res.json(topic);
    });
});

/* POST add topic comment */
router.post('/topics/:topic/comments', function (req, res, next) {
    var comment = new Comment(req.body);
    comment.topic = req.topic;

    comment.save(function (err, comment) {
        if (err) return next(err);

        req.topic.comments.push(comment);
        req.topic.save(function (err, topic) {
            if (err) return next(err);

            res.json(comment);
        });
    });
});

/* PUT topic comment upvote */
router.put('/comments/:comment/upvote', function (req, res, next) {
    req.comment.upvote(function (err, comment) {
        if (err) return next(err);

        res.json(comment);
    });
});

/* GET comments */
router.get('/comments/:comment', function (req, res) {
    res.json(req.comment);
});

/* PRELOADING topics */
router.param('topic', function (req, res, next, id) {
    var query = Topic.findById(id);

    query.exec(function (err, topic) {
        if (err) return next(err);

        if (!topic) return next(new Error('can\'t find topic'));

        req.topic = topic;
        return next();
    })
});

/* PRELOADING topic comments */
router.param('comment', function (req, res, next, id) {
    var query = Comment.findById(id);

    query.exec(function (err, comment) {
        if (err) return next(err);

        if (!comment)
            return next(new Error('can\'t find comment'));

        req.comment = comment;
        return next();
    })
});

module.exports = router;
