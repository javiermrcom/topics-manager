angular
    .module('topicsManager')
    .controller('MainCtrl', MainCtrl);

MainCtrl.$inject = ['topics', 'topicsService'];
function MainCtrl(topics, topicsService) {
    var vm = this;

    vm.addTopic = addTopic;
    vm.vote = vote;
    vm.topics = topicsService.topics;

    // -----------------------------------

    function addTopic() {
        if (!vm.title || vm.title == '') return;

        topicsService.create({
            title: vm.title && vm.title != '' ? vm.title : 'No title',
            link: vm.link && vm.link != '' ? vm.link : null,
            upvotes: 0
        });

        vm.title = '';
        vm.link = '';
    }

    function vote(topic) {
        topicsService.upvote(topic);
    }
}
