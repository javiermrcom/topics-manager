angular
    .module('topicsManager')
    .controller('TopicCtrl', TopicCtrl);

TopicCtrl.$inject = ['topic', 'topicsService'];
function TopicCtrl(topic, topicsService) {
    var vm = this;

    vm.topic = topic;

    vm.vote = vote;
    vm.addComment = addComment;

    // -----------------------------------

    function vote(comments) {
        comments.upvotes++;
    }

    function addComment() {
        if (!vm.body || vm.body == '') return;

        topicsService.addComment(vm.topic._id, {
            body: vm.body,
            author: 'user'
        }).then(function (data) {
            console.log(data.data);
            vm.topic.comments.push(data.data);
        });

        vm.body = '';
    }

}
