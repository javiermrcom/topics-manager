angular
    .module('topicsManager')
    .factory('topicsService', topicsService);

topicsService.$inject = ['$http'];
function topicsService($http) {

    var service = {
        'topics': [],
        'getAll': getAll,
        'create': create,
        'upvote': upvote,
        'get': get,
        'addComment': addComment
    };

    return service;

    // ------------------------------------

    function getAll() {
        return $http.get('/topics')
            .then(function (data) {
                console.log(data.data);
                angular.copy(data.data, service.topics);
            });
    }

    function create(topic) {
        return $http.post('/topics', topic)
            .then(function (data) {
                service.topics.push(data);
            });
    }

    function upvote(topic) {
        return $http.put('/topics/' + topic._id + '/upvote')
            .then(function (data) {
                topic.upvotes += 1;
            });
    }

    function get(id) {
        return $http.get('/topics/' + id)
            .then(function (res) {
                return res.data;
            });
    }

    function addComment(id, comment) {
        return $http.post('/topics/' + id + '/comments', comment);
    }
}