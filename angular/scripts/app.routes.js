angular
    .module('topicsManager')
    .config(routes);

routes.$inject = [
    '$stateProvider',
    '$urlRouterProvider'
];
function routes($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: '/views/home.tpl.html',
            controller: 'MainCtrl',
            controllerAs: 'vm',
            resolve: {
                topics: function (topicsService) {
                    return topicsService.getAll();
                }
            }
        })
        .state('topics', {
            url: '/topics/:id',
            templateUrl: '/views/topic.tpl.html',
            controller: 'TopicCtrl',
            controllerAs: 'vm',
            resolve: {
                topic: function (topicsService, $stateParams) {
                    return topicsService.get($stateParams.id);
                }
            }
        });

    $urlRouterProvider.otherwise('home');
}
